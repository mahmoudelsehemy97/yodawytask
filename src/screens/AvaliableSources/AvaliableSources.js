import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  TouchableOpacity,
  Alert
} from 'react-native';
import axios from 'axios';
import { API_ENDPOINT, API_KEY } from '../../configs';
import { RNToasty } from 'react-native-toasty';
import colors from '../../assets/colors'
import Header from '../../components/Header';

class AvaliableSources extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: ''
    };

  }
  componentDidMount() {
    axios.get(`${API_ENDPOINT}/sources`
      , {
        params: {
          language: 'en',
          country: 'us',
          apiKey: `${API_KEY}`
        }
      }
    )
      .then((response) => {
        this.setState({ data: response.data.sources })
      })
      .catch((error) => {
        RNToasty.Error({
          title: error + ""
        });
      })
  }
  render() {
    const { data } = this.state;
    const { props } = this.props;

    return (
      <>
        <StatusBar barStyle="light-content" backgroundColor={colors.statusBar} />
        <Header title={"Availble Recources"}></Header>
        <FlatList style={{ backgroundColor: 'white', width: '100%' }}
          data={data}
          keyExtractor={({ id }, index) => id}
          renderItem={({ item }) =>
            <TouchableOpacity onPress={() => props.navigation.navigate("HeadlinesScreen", { id: item.id, name: item.name })}>
              <Text style={{ padding: 10, color: colors.primary, fontSize: 15 }}>{item.name}</Text>
            </TouchableOpacity>
          }>
        </FlatList>
      </>
    );
  };
}

const styles = StyleSheet.create({

});

export default AvaliableSources;
