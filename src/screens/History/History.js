import React from 'react';
import {
  FlatList,
  Text,
  StatusBar,
} from 'react-native';
import colors from '../../assets/colors';
import { connect } from 'react-redux';
import { Card } from 'react-native-elements'

class History extends React.Component {

  render() {
    return (
      <>
        <StatusBar
          barStyle="light-content"
          backgroundColor={colors.statusBar}
        />
        <FlatList
          style={{ backgroundColor: 'white', width: '100%' }}
          data={this.props.historyItems}
          keyExtractor={(item) => item.title}
          renderItem={({ item }) =>

            <Card
              title={JSON.stringify(item.title)}
              key={JSON.stringify(item.data)} >

              <Text style={{ marginBottom: 10 }}>
                Author Name : <Text> {item.author ? JSON.stringify(item.author) : "Not Mentioned"}</Text>
              </Text>
              <Text style={{ marginBottom: 10 }}>
                Source Name : <Text> {item.source.name ? JSON.stringify(item.source.name) : "Not Mentioned"}</Text>
              </Text>
              <Text style={{ marginBottom: 10 }}>
                Date : <Text> {item.data ? JSON.stringify(item.data) : "Not Mentioned"}</Text>
              </Text>
            </Card>
          }>
        </FlatList>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    historyItems: state.history.historyItems,
  };
};

export default connect(mapStateToProps)(History);
