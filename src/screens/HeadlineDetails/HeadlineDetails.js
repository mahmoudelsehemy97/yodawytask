import React from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Alert,
    Image,
    Linking,
    StatusBar
} from 'react-native';
import Header from '../../components/Header'
import colors from '../../assets/colors'

class HeadlineDetails extends React.Component {
    render() {
        // console.log(this.props.route.params.item)
        const { title, urlToImage, publishedAt, content, author, url } = this.props.route.params.item
        // console.log(title)
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <StatusBar barStyle="light-content" backgroundColor={colors.statusBar} />
                <Header showBack props={this.props}></Header>
                <ScrollView>
                    <Text style={{ marginVertical: 10, fontSize: 17, marginHorizontal: 10, textAlign: 'center', fontWeight: 'bold' }}>{title ? title : "NO TITLE ABAILABLE"}</Text>
                    <Image
                        style={{ width: '100%', height: 270, }}
                        source={{ uri: urlToImage ? urlToImage : "https://www.bounceorlando.com/cp/resources/images//items/no_picture.png" }}></Image>
                    <Text style={styles.text}>Author Name: <Text style={styles.inText}>{author ? author : "NOT MENTIONED"}</Text></Text>
                    <Text style={styles.text}>Published At:<Text style={styles.inText}>{publishedAt ? publishedAt : "NOT MENTIONED"}</Text></Text>
                    <Text style={styles.text} >Content: <Text style={styles.inText}>{content ? content : "NOT AVAILABLE"}</Text></Text>
                    <TouchableOpacity style={{ marginVertical: 10, marginHorizontal: 10, borderRadius: 25, height: 45, justifyContent: 'center', alignItems: 'center', backgroundColor: colors.secondary }}
                        onPress={() => Linking.openURL(url)}>
                        <Text style={{ color: 'white', fontSize: 17 }}>Go To The Article</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    text: {
        marginVertical: 10,
        fontSize: 17,
        marginHorizontal: 10,
        fontWeight: 'bold'
    },
    inText: {
        fontSize: 16,
        fontWeight: 'normal'
    }
});

export default HeadlineDetails;
