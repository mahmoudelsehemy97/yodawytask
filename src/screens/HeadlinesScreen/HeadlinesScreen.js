import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Alert,
  Text,
  StatusBar,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Headline from '../../components/Headline';
import axios from 'axios';
import { API_ENDPOINT, API_KEY } from '../../configs';
import Header from '../../components/Header';
import { RNToasty } from 'react-native-toasty';
import colors from '../../assets/colors';
import { addItemToHistory } from '../../redux/actions/addHistrory';
import { connect } from 'react-redux';

class HeadlinesScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 'business',
      date: '',
      ON: true,
    };
  }
  getDate = () => {
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    this.setState({
      //Setting the value of the date time
      date:
        date + '/' + month + '/' + year + ' ' + hours + ':' + min + ':' + sec,
    });
  };
  componentDidMount() {
    this.getDate();
    this.api_request();
  }
  api_request = () => {
    const id =
      this.props.type == 'TOP_HEADLINES' ? null : this.props.route.params.id;
    {
      this.props.type == 'TOP_HEADLINES'
        ? axios
          .get(`${API_ENDPOINT}/top-headlines`, {
            params: {
              country: 'ae',
              category: this.state.page,
              apiKey: `${API_KEY}`,
            },
          })
          .then((response) => {
            this.setState({ data: response.data.articles });
            // console.log(this.state.data)
          })
          .catch((error) => {
            RNToasty.Error({
              title: error + '',
            });
          })
        : axios
          .get(`${API_ENDPOINT}/top-headlines?sources=${id}`, {
            params: {
              apiKey: `${API_KEY}`,
            },
          })
          .then((response) => {
            this.setState({ data: response.data.articles });
            // console.log(this.state.data)
          })
          .catch((error) => {
            RNToasty.Error({
              title: error + '',
            });
          });
    }
  };

  render() {
    const { data, page } = this.state;
    const { navigation } =
      this.props.type == 'TOP_HEADLINES' ? this.props.props : this.props;
    return (
      <View style={{ backgroundColor: 'transparent' }}>
        <StatusBar
          barStyle="light-content"
          backgroundColor={colors.statusBar}
        />
        <Header
          title={
            this.props.type == 'TOP_HEADLINES'
              ? 'Top Headlines '
              : this.props.route.params.name + ' Headlines'
          }
          showBack={this.props.type == 'TOP_HEADLINES' ? null : true}
          props={this.props}></Header>

        {this.props.type == 'TOP_HEADLINES' && (
          <View
            style={{
              borderBottomWidth: 0.2,
              borderBottomColor: colors.gray,
              justifyContent: 'space-around',
              flexDirection: 'row',
              paddingVertical: 10,
              backgroundColor: 'white',
            }}>
            <TouchableOpacity
              onPress={() => {
                this.setState({ page: 'business' }, () => this.api_request());

              }}
              style={{
                borderColor: colors.primary,
                borderWidth: 1,
                paddingHorizontal: 20,
                paddingVertical: 4,
                backgroundColor: page == 'business' ? colors.primary : 'white',
                borderRadius: 20,
              }}>
              <Text
                style={{ fontSize: 17, color: page == 'business' ? 'white' : colors.primary, }}>
                business
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.setState({ page: 'sports' }, () => this.api_request());
              }}
              style={{
                borderColor: colors.primary,
                borderWidth: 1,
                paddingHorizontal: 20,
                paddingVertical: 4,
                backgroundColor: page == 'sports' ? colors.primary : 'white',
                borderRadius: 20,
              }}>
              <Text
                style={{ fontSize: 17, color: page == 'sports' ? 'white' : colors.primary, }}>
                sports
              </Text>
            </TouchableOpacity>
          </View>
        )}

        <FlatList
          style={{ backgroundColor: 'white', width: '100%' }}
          data={data}
          keyExtractor={(item) => item.title}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() => {
                this.getDate();
                let date = this.state.date;
                this.props.addItemToHistory({ ...item, data: date });
                navigation.navigate('HeadlineDetails', { item: item });

              }}>
              <Headline
                title={item.title ? item.title : 'NO TITLE'}
                imageUrl={
                  item.urlToImage
                    ? item.urlToImage
                    : 'https://www.bounceorlando.com/cp/resources/images//items/no_picture.png'
                }
                autherName={item.author ? item.author : 'NOT MENTIONED'}
                date={item.publishedAt}></Headline>
            </TouchableOpacity>
          )}></FlatList>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    // historyItems: state.history.historyItems,
  };
};

export default connect(mapStateToProps, { addItemToHistory })(HeadlinesScreen);
