import React from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    TouchableOpacity,
    Alert,
    Image,

} from 'react-native';
import { Card } from 'react-native-elements'

// Api_key = "56a1e4d2bad64c0880bbd187e15240d1"
class Headline extends React.Component {

    render() {
        const { title, imageUrl, autherName, date } = this.props;

        return (
            <Card
                title={title}
                image={{ uri: imageUrl }}
                key={title}
            >
                <Text >Author Name:<Text > {autherName}</Text></Text>
                <Text>Published At:<Text> {date}</Text></Text>
            </Card>
        );
    };
}

const styles = StyleSheet.create({

});

export default Headline;
