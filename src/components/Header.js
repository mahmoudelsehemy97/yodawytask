import React from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    Alert,
    TouchableOpacity,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import colors from '../assets/colors'
import { StackActions } from '@react-navigation/native';

class Header extends React.Component {
    render() {
        const { title, showBack } = this.props
        return (
            <View style={{ justifyContent: 'space-between', alignItems: 'center', height: 50, flexDirection: 'row', backgroundColor: colors.primary }}>
                {
                    showBack
                        ?
                        <TouchableOpacity onPress={() => this.props.props.navigation.dispatch(StackActions.pop(1))} style={{ marginLeft: 15 }}>
                            <Ionicons
                                name='ios-arrow-round-back'
                                color="white"
                                size={40}
                            />
                        </TouchableOpacity>
                        :
                        <Text></Text>
                }
                <Text style={{ fontSize: 20, color: 'white', fontWeight: 'bold' }}>{title}</Text>
                <Text></Text>

            </View >
        );
    };
}

const styles = StyleSheet.create({

});

export default Header;
