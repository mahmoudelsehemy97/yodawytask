import { ADD_TO_HISTORY } from './types';
let nextId = 0

export function addItemToHistory(item) {
 // console.log('item    ', item);
  return (dispatch) => {
    dispatch({ type: ADD_TO_HISTORY, payload: item , id: nextId++, });
  };
}
