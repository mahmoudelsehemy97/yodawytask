import * as types from '../actions/types';

const initialState = {
  historyItems: [],
};

const historyReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_TO_HISTORY: {

      // for (let i = 0; i < state.historyItems.length; i++) {
      //   if (state.historyItems[i].title === action.payload.title) {
      //     console.log("LOOG     " + JSON.stringify(state.historyItems[i]))
      //     state.historyItems.splice(i, 1);
      //     state.historyItems.unshift(state.historyItems[i]);
      //   }
      // }
      state.historyItems.forEach(function (item, i) {
        if (item.title === action.payload.title) {
          state.historyItems.pop(item);
         // state.historyItems.push(item);
       
        }
    
       
      });
    
        return {
          ...state, historyItems: [...state.historyItems, action.payload].reverse()
        };
    }
    default:
      return state;
  }
};

export default historyReducer;
