import historyReducer from './historyReducer';

import {combineReducers} from 'redux';

export default combineReducers({
  history: historyReducer,
});
