import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  BackHandler,
  StatusBar,
} from 'react-native';
import Tabs from 'react-native-tabs';
import HeadlinesScreen from './screens/HeadlinesScreen/HeadlinesScreen';
import AvaliableSources from './screens/AvaliableSources/AvaliableSources';
import History from './screens/History/History';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import colors from './assets/colors';
import {RNToasty} from 'react-native-toasty';
import SplashScreen from 'react-native-splash-screen';

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 'HeadlinesScreen',
    };
    SplashScreen.hide();
  }
  render() {
    const {page} = this.state;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#F5FCFF',
        }}>
        <View style={{flex: 1}}>
          {page == 'HeadlinesScreen' && (
            <HeadlinesScreen props={this.props} type={'TOP_HEADLINES'} />
          )}
          {page == 'AvaliableSources' && (
            <AvaliableSources props={this.props} />
          )}
          {page == 'History' && <History props={this.props} />}
        </View>
        <Tabs
          selected={this.state.page}
          swipeEnabled={true}
          style={{height: 70, position: 'relative', backgroundColor: 'white'}}
          selectedStyle={{color: '#39B54A'}}
          onSelect={(el) => this.setState({page: el.props.name})}>
          <View
            name={'HeadlinesScreen'}
            style={styles.container}>
            <FontAwesome
              name={'newspaper-o'}
              color={page == 'HeadlinesScreen' ? colors.primary : colors.gray}
              size={25}
              style={{flex: 1, justifyContent: 'center', alignItems: 'stretch'}}
            />
            <Text
              style={{
                flex: 1,
                alignItems: 'stretch',
                justifyContent: 'center',
                color: page == 'HeadlinesScreen' ? colors.primary : colors.gray,
              }}>
              Top Headlines
            </Text>
          </View>
          <View
             style={styles.container}
            name={'AvaliableSources'}>
            <FontAwesome
              name={'search'}
              color={page == 'AvaliableSources' ? colors.primary : colors.gray}
              size={25}
            />
            <Text
              style={{
                flex: 1,
                alignItems: 'stretch',
                justifyContent: 'center',
                color:
                  page == 'AvaliableSources' ? colors.primary : colors.gray,
              }}>
              Avaliable Sources
            </Text>
          </View>
          <View
            style={styles.container}
            name={'History'}>
            <FontAwesome
              name={'history'}
              color={page == 'History' ? colors.primary : colors.gray}
              size={25}
            />
            <Text
              style={{
                flex: 1,
                alignItems: 'stretch',
                justifyContent: 'center',
                color: page == 'History' ? colors.primary : colors.gray,
              }}>
              History
            </Text>
          </View>
        </Tabs>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

export default Main;
