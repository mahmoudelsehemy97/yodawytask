## brief

This project creates an app to call multiple apis from [News API](https://newsapi.org/) , Show them, Save and Manipulate.


## Used Libraries

[Redux](https://redux.js.org) Redux is a state management tool,the state of your application is kept in a store, and each component can access any state that it needs from this store.

 [React Navigation](https://reactnavigation.org) Reat Navigation used for Routing and navigation for your app.

 [react-native-tabs](https://github.com/aksonov/react-native-tabs)

 [react-native-toasty](https://www.npmjs.com/package/react-native-toasty)

 [react-native-vector-icons](https://github.com/oblador/react-native-vector-icons)

## Descriptipn

The application consists of 3 main tabs.

Headlines Screen , Availbale Resources , History. 
 
## Headlines Screen 

first tab shows top headlines from the news Api using a flatList and render a component called Headline.
when the user clicks on any headline it navigates using react-navigation to a new screen called HeadlineDetails. 
the HeadlineDetails shows headline information like image, title, author name,date and time for publishing,the content and a button which
navigates the user to external web browser to the choosen article.

## Availbale Resources

second tab shows all resources from the same Api using a flatList.
when the user clicks on any source it navigated to a screen shows top headlines for that source like in Headlines Screen mentioned before.

## History 

third tab shows the headlines the user clicked in reserve chronological order.