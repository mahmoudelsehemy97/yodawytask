import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();
import Main from './src/main';
import AvaliableSources from './src/screens/AvaliableSources/AvaliableSources';
import History from './src/screens/History/History';
import HeadlineDetails from './src/screens/HeadlineDetails/HeadlineDetails';
import HeadlinesScreen from './src/screens/HeadlinesScreen/HeadlinesScreen';
import {Provider} from 'react-redux';
import store from './src/redux/store';
class App extends React.Component {
  render() {
    return (
      <>
        <Provider store={store}>
          <NavigationContainer>
            <Stack.Navigator initialRouteName="Main" headerMode="none">
              <Stack.Screen name="Main" component={Main} />
              <Stack.Screen
                name="AvaliableSources"
                component={AvaliableSources}
              />
              <Stack.Screen name="History" component={History} />
              <Stack.Screen
                name="HeadlineDetails"
                component={HeadlineDetails}
              />
              <Stack.Screen
                name="HeadlinesScreen"
                component={HeadlinesScreen}
              />
            </Stack.Navigator>
          </NavigationContainer>
        </Provider>
      </>
    );
  }
}

const styles = StyleSheet.create({});

export default App;
